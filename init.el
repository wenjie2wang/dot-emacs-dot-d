;;; package --- Summary
;;; Commentary:
;;; Code:

(defconst *is-a-mac* (eq system-type 'darwin))
;; Suppress GUI features
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(setq inhibit-startup-screen t)
;; inhibit welcome page
(setq inhibit-startup-message t)
(setq-default
 initial-scratch-message
 (concat ";; Happy hacking, "user-login-name " - Emacs ♥ you!\n\n"))

;; show column number
(setq column-number-mode t)
;; no menu bar
(menu-bar-mode -1)

;; set margin
;; (setq left-margin-width 1)
;; (setq right-margin-width 1)
;; (setq left-fringe-width 1)
;; (setq right-fringe-width 1)

;;; set default font
(if *is-a-mac*
    (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-16"))
  (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-12")))

(setq browse-url-new-window-flag t)
(if *is-a-mac*
    (setq browse-url-browser-function 'browse-url-default-macosx-browser)
  (setq browse-url-browser-function 'browse-url-firefox
        browse-url-firefox-new-window-is-tab t))

;; load config under `lisp/`
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;; Window size and features
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'set-scroll-bar-mode)
  (set-scroll-bar-mode nil))

(if *is-a-mac*
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (unless (display-graphic-p frame)
                  (set-frame-parameter frame 'menu-bar-lines 0))))
  (when (fboundp 'menu-bar-mode)
    (menu-bar-mode -1)))

(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

;; get rid of gaps
;; Non-nil means resize frames pixelwise.
;; If this option is nil, resizing a frame rounds its sizes to the frame's
;; current values of `frame-char-height' and `frame-char-width'.  If this
;; is non-nil, no rounding occurs, hence frame sizes can increase/decrease
;; by one pixel.
;; With some window managers you have to set this to non-nil in order to
;; fully maximize frames.  To resize your initial frame pixelwise,
;; set this option to a non-nil value in your init file.
(setq frame-resize-pixelwise t)

(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))
;; (setq frame-title-format "%b")

;; Allow access from emacsclient
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))

;; ;; native compilation
(setq package-native-compile t)
(setq native-comp-async-report-warnings-errors 'silent)
;; (setq warning-minimum-level :error)

;; case-insensitive when find-file and switch-to-buffer
(setq read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t
      completion-ignore-case t)

;; load other initialization configs
(require 'init-straight)
;; (require 'init-elpa)

(require 'init-window)
(require 'init-theme)
(require 'init-osx)
(require 'init-tramp)
(require 'init-git)

(require 'init-corfu)
(require 'init-eglot)
(require 'init-ibuffer)
(require 'init-isearch)
(require 'init-projectile)
(require 'init-mini)
;; (require 'init-ivy)
(require 'init-whitespace)
(require 'init-modeline)

(require 'init-tags)
(require 'init-term)
(require 'init-edit)
;; (require 'init-tree-sitter)
(require 'init-snippet)
(require 'init-org)

(require 'init-cc)
(require 'init-julia)
(require 'init-ess)
(require 'init-python)
(require 'init-poly)
(require 'init-latex)
(require 'init-docker)
(require 'init-doxygen)
(require 'init-web)
(require 'init-yaml)
;; (require 'init-flycheck)
(require 'init-spell)
(require 'init-pass)

;; (require 'init-matlab)
(require 'init-langtool)
(require 'init-bibtex)
(require 'init-pdf)
(require 'init-everywhere)
(require 'init-llm)

(if *is-a-mac* nil
  (require 'init-chinese)
  (require 'init-mu4e)
  )

(require 'init-recentf)

(provide 'init)
;;; init ends here
