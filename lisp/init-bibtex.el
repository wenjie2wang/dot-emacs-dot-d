(use-package bibtex
  :config
  (setq bibtex-autokey-year-length 4
        bibtex-autokey-name-year-separator ""
        bibtex-autokey-year-title-separator ""
        bibtex-autokey-titleword-separator "-"
        bibtex-autokey-titlewords 1
        bibtex-autokey-titlewords-stretch 1
        bibtex-autokey-titleword-length 5))

;;; auto run bib-validate-globally whenever saving bibtex files
(defun bibtex-auto-validate-globally ()
  "Run bibtex-validate-globally after saving the bibtex files."
  (when (eq major-mode 'bibtex-mode)
    (bibtex-validate-globally)))
(add-hook 'after-save-hook #'bibtex-auto-validate-globally)

;;; using gsholar-bibtex
(use-package gscholar-bibtex
  :config
  (setq gscholar-bibtex-default-source "Google Scholar")
  (if (file-exists-p "~/bibrary/pending")
      (setq gscholar-bibtex-database-file "~/bibrary/pending/index.bib")))

(use-package biblio)
(use-package bibtex-completion)

(when (file-exists-p "~/bibrary")
  (setq bibtex-completion-bibliography
        '("~/bibrary/bib/articles.bib"
          "~/bibrary/bib/books.bib"
          "~/bibrary/bib/others.bib"))
  (setq bibtex-completion-library-path
        '("~/bibrary/articles"
          "~/bibrary/books"
          "~/bibrary/others"
          "~/bibrary/pending")))

;; notes
;; (if (file-exists-p "~/git/org-my-life/")
;;     (setq bibtex-completion-notes-path "~/git/org-my-life/org/bibtex.org"))

;; "\n* ${author-or-editor} (${year}): ${title}\n  :PROPERTIES:\n  :Custom_ID: ${=key=}\n  :END:\n\n"
;; (setq bibtex-completion-notes-template-one-file
;;   "\n* ${=key=}:\n  :PROPERTIES:\n  :Custom_ID: ${=key=}\n  :TITLE: ${title}\n  :AUTHOR: ${author-or-editor}\n  :YEAR: ${year}\n  :END:\n\n")
(setq bibtex-completion-pdf-field nil)
(setq bibtex-completion-find-additional-pdfs t)
(setq bibtex-completion-pdf-extension '(".pdf"))
(if (executable-find "okular")
    (setq bibtex-completion-pdf-open-function
          (lambda (fpath)
            (call-process "okular" nil 0 nil fpath))))

(use-package ivy-bibtex
  :config
  (global-set-key (kbd "M-s b") 'ivy-bibtex))

;; use citar
;; (use-package citar
;;   :ensure t
;;   :bind (("M-s b" . citar-open))
;;   :custom
;;   (citar-display-transform-functions
;;    '((("author" "editor") . my-citar--shorten-names)))
;;   (citar-bibliography '("~/bibrary/bib/index.bib"))
;;   (citar-library-paths '("~/bibrary/papers"
;;                          "~/bibrary/books"
;;                          "~/bibrary/misc"))
;;   (citar-library-file-extensions (list "pdf" "djvu"))
;;   (citar-file-additional-files-separator "-supplementary")
;;   (citar-notes-paths '("~/org-my-life/bibtex"))
;;   ;; (citar-file-open-functions )
;;   (citar-templates
;;    '((main . "${author editor:32}    ${date year issued:4}    ${title:80}")
;;      (suffix . "    ${=type=:12}    ${=key= id:30}")
;;      ;; (preview . "${author editor} (${year issued date}) ${title}, ${journal journaltitle publisher container-title collection-title}.\n")
;;      ;; (note . "Notes on ${author editor}, ${title}")
;;      (preview . "")
;;      (note . "")
;;      ))
;;   (citar-symbols
;;    `((file ,(all-the-icons-octicon "file-pdf" :face 'all-the-icons-red :v-adjust -0.1) . " ")
;;      (note ,(all-the-icons-material "speaker_notes" :face 'all-the-icons-blue :v-adjust -0.3) . " ")
;;      (link ,(all-the-icons-octicon "link" :face 'all-the-icons-orange :v-adjust 0.01) . " ")))
;;   (citar-symbol-separator "  ")
;;   )

;; (unless *is-a-mac*
;;   (setq citar-file-open-functions
;;         '((t . citar-file-open-external))))

;; (use-package citar-embark
;;   :ensure t
;;   :after citar embark
;;   :no-require
;;   :config (citar-embark-mode))

;; pkg: org-ref
(use-package org-ref
  :config
  (setq org-ref-pdf-to-bibtex-function 'rename-file))

;; pkg: org-roam-bibtex
(use-package org-roam-bibtex
  :after org-roam
  :config
  (org-roam-bibtex-mode 1))

(provide 'init-bibtex)
