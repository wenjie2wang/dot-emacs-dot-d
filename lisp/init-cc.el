;;; package --- Summary:
;;; Commentary:
;;; Code:

;;; using another indentation style
(c-add-style "ellemtel-offset4"
             '("ellemtel"
               (indent-tabs-mode . nil)        ; use spaces rather than tabs
               (c-basic-offset . 4)            ; indent by four spaces
               (c-indent-level . 4)
               ))

(defun my-c++-mode-hook ()
  "Use my-style defined above."
  (c-set-style "ellemtel-offset4")
  )
(add-hook 'c++-mode-hook 'my-c++-mode-hook)

;;; use c++-mode by default for .h files
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;; set R-lib
(setq r-lib-suffix "/usr/local/lib/R/site-library/")
(setq r-lib-path "/usr/include/R/")
(when *is-a-mac*
  (setq r-lib-suffix "/Library/Frameworks/R.framework/Versions/4.4/Resources/library/")
  (setq r-lib-path "/Library/Frameworks/R.framework/Resources/include/"))

;;; flycheck
(add-hook 'c++-mode-hook
          (lambda ()
            (setq flycheck-gcc-language-standard "c++17")
            (setq flycheck-clang-language-standard "c++17")
            ;; (setq flycheck-disabled-checkers '(c/c++-clang))
            ;; (setq flycheck-select-checker "c/c++-gcc")
            (setq flycheck-select-checker "c/c++-clang")
            ;; add R, Rcpp, and its friends
            (setq flycheck-clang-include-path
                  (list
                   r-lib-path
                   (concat r-lib-suffix "Rcpp/include")
                   (concat r-lib-suffix "RcppArmadillo/include")
                   (concat r-lib-suffix "RcppEigen/include")
                   )
                  )
            )
          )

;; cuda
(use-package cuda-mode)

;; rust
(use-package rust-mode)

(provide 'init-cc)
;;; init-cc.el ends here
