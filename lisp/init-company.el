;; pkg: company
(use-package company
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  :config
  (define-key company-mode-map (kbd "M-/") 'company-complete)
  (define-key company-active-map (kbd "M-/") 'company-other-backend)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (setq-default company-dabbrev-other-buffers 'all
                company-tooltip-align-annotations t)
  (global-set-key (kbd "M-C-/") 'company-complete)
  (setq tab-always-indent 'complete)
  (add-to-list 'completion-styles 'initials t)
  (setq company-selection-wrap-around t
        company-tooltip-align-annotations t
        company-idle-delay 0.3
        company-minimum-prefix-length 2
        company-tooltip-limit 10)
  )

(provide 'init-company)
