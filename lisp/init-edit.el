(global-set-key (kbd "RET") 'newline-and-indent)
;;; set global key for most programming languages
(global-set-key (kbd "M--") " = ")
;; (setq require-final-newline t)

;; hl-line
(add-hook 'prog-mode-hook 'hl-line-mode)

(when (fboundp 'electric-pair-mode)
  (add-hook 'after-init-hook 'electric-pair-mode))

(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when *is-a-mac*
  (setq-default locate-command "mdfind"))

;; For auto-fill-mode
(setq-default fill-column 80)

;; highlight pairs
(add-hook 'after-init-hook 'show-paren-mode)

(setq-default
 blink-cursor-interval 0.4
 bookmark-default-file (expand-file-name ".bookmarks.el" user-emacs-directory)
 buffers-menu-max-size 30
 case-fold-search t ;; case insensitive search by default
 column-number-mode t
 delete-selection-mode t
 ediff-split-window-function 'split-window-horizontally
 ediff-window-setup-function 'ediff-setup-windows-plain
 indent-tabs-mode nil
 tab-width 4
 create-lockfiles nil
 auto-save-default nil
 make-backup-files nil
 mouse-yank-at-point t
 save-interprogram-paste-before-kill t
 scroll-preserve-screen-position 'always
 set-mark-command-repeat-pop t
 tooltip-delay 1.5
 truncate-lines nil
 truncate-partial-width-windows nil)

(add-hook 'after-init-hook 'global-auto-revert-mode)
(setq-default global-auto-revert-non-file-buffers t
              auto-revert-verbose nil)

;; Rectangle selections, and overwrite text when the selection is active
(cua-selection-mode t)
;; disable cua rectangle mark
(define-key cua-global-keymap [C-return] nil)

;; fill one sentence at one time
(defun fill-one-sentence ()
  "Fill the current sentence only."
  (interactive)
  (save-excursion
    (backward-sentence)
    (push-mark)
    (forward-sentence)
    (if (eq major-mode 'org-mode)
        (org-fill-region (point) (mark))
      (fill-region (point) (mark))
      )
    )
  )

;; modified from variuos online examples
(defun unfill-sentences ()
  "Unfill sentences such that one sentence per line."
  (interactive)
  (save-excursion
    (unfill-paragraph)
    (let ((end (save-excursion
                 (forward-paragraph 1)
                 (backward-sentence)
                 (point-marker))))
      (beginning-of-line)
      (while (progn (forward-sentence)
                    (<= (point) (marker-position end)))
        (just-one-space) ;; leaves only one space, point is after it
        (delete-char -1) ;; delete the space
        (newline)        ;; and insert a newline
        (if (eq major-mode 'LaTeX-mode)
            (LaTeX-indent-line) nil)
        ))
    ))

;; it seems that it freezes emacs when it is called at end of the file
;; because there is no next paragraph?
(defun fill-sentences ()
  "Fill sentences by double space after a period."
  (interactive)
  (save-excursion
    (unfill-paragraph)
    (let ((end (save-excursion
                 (forward-paragraph 1)
                 ;; (backward-sentence)
                 (point-marker))))  ;; remember where to stop
      (beginning-of-line)
      (while (progn (forward-sentence)
                    (<= (point) (marker-position end)))
        (just-one-space) ;; leaves only one space, point is after it
        (delete-char -1) ;; delete the space
        (newline)        ;; and insert a newline
        (if (eq major-mode 'LaTeX-mode)
            (LaTeX-indent-line) nil)
        ;; fill sentence
        (push-mark)
        (backward-sentence)
        ;; workaround for \end{environment} in latex
        (if (or (and (eq (char-after) ?\\)
                 (eq (char-after (+ 1 (point))) ?e)
                 (eq (char-after (+ 2 (point))) ?n)
                 (eq (char-after (+ 3 (point))) ?d))
                (and (eq (char-after) ?\\)
                 (eq (char-after (+ 1 (point))) ?b)
                 (eq (char-after (+ 2 (point))) ?e)
                 (eq (char-after (+ 3 (point))) ?g)))
            (next-line) nil)
        (fill-region (point) (mark))
        (forward-sentence 2)
        (backward-sentence)
        )
      (backward-paragraph)
      (if (eq (char-after) ?\n)
          (delete-char 1))
      )
    ))
;; (global-set-key (kbd "M-q") 'fill-sentences)

;; for visual line mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;; for bash scripts
(add-to-list 'auto-mode-alist '("\\.sh$" . shell-script-mode))

;; (use-package undo-tree
;;   :config
;;   (setq undo-tree-auto-save-history nil)
;;   (global-undo-tree-mode))

;; to quickly revert-buffer without confirmation
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive) (revert-buffer t t)
  (message "Reverted buffer"))
(global-set-key (kbd "C-c C-q") 'revert-buffer-no-confirm)

;; pkg: unfill
(use-package unfill)

;; pkg: vlf
(use-package vlf
  :config
  (defun ffap-vlf ()
  "Find file at point with VLF."
  (interactive)
  (let ((file (ffap-file-at-point)))
    (unless (file-exists-p file)
      (error "File does not exist: %s" file))
    (vlf file)))
  )

;; multiple-cursors
(use-package multiple-cursors
  :config
  (define-key mc/keymap (kbd "<return>") nil) ; use C-g to quit only
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-+") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
  ;; From active region to multiple cursors:
  ;; (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
  )

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package symbol-overlay
  :config
  (dolist (hook '(prog-mode-hook html-mode-hook yaml-mode-hook conf-mode-hook))
    (add-hook hook 'symbol-overlay-mode))
  (define-key symbol-overlay-mode-map (kbd "M-i") 'symbol-overlay-put)
  (define-key symbol-overlay-mode-map (kbd "M-I") 'symbol-overlay-remove-all)
  (define-key symbol-overlay-mode-map (kbd "M-n") 'symbol-overlay-jump-next)
  (define-key symbol-overlay-mode-map (kbd "M-p") 'symbol-overlay-jump-prev))

(use-package column-enforce-mode
  :config
  (setq column-enforce-comments nil)
  (setq column-enforce-column 80)
  (add-hook 'prog-mode-hook 'column-enforce-mode)
  ;; (add-hook 'ess-mode-hook (lambda () (interactive) (column-enforce-mode)))
  ;; (add-hook 'c++-mode-hook (lambda () (interactive) (column-enforce-mode)))
  )

(use-package disable-mouse)

(use-package editorconfig
  :config
  (editorconfig-mode 1)
  ;; hide mode line
  (setq-default editorconfig-mode-lighter ""))

(provide 'init-edit)
;;; init-edit.el ends here
