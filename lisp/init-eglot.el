;;; init-eglot.el --- Config for eglot
;;; Commentary:
;;; Code:

(require 'flymake)
;; for Emacs 30.1+
(setq flymake-indicator-type 'fringes
      flymake-autoresize-margins nil)

;; (use-package eglot
;;   :config
(require 'eglot)
  ;; disable hover-provider in mini buffer
  (setq eglot-ignored-server-capabilities '(:hoverProvider))
  (setq eglot-autoshutdown t)
  (setq eglot-autoreconnect nil)        ; never auto reconnect
  ;; key bindings
  (define-key eglot-mode-map (kbd "C-c r") 'eglot-rename)
  (define-key eglot-mode-map (kbd "C-c h") 'eldoc)
  ;; C/C++: clangd
  (add-to-list 'eglot-server-programs
               '((c++-mode c-mode)
                 . ("clangd"
                    "-j=2"
                    "--header-insertion=never"
                    "--header-insertion-decorators=0")))
  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  ;; R: languageserver package
  ;; (add-hook 'ess-mode-hook 'eglot-ensure)
  ;; (add-hook 'ess-r-mode-hook 'eglot-ensure)
  ;; Python: pyright
  (add-hook 'python-mode-hook 'eglot-ensure)
  ;; (add-to-list 'eglot-stay-out-of 'cython-mode)
  ;; LaTeX: digestif
  ;; (add-hook 'latex-mode-hook 'eglot-ensure)
  ;; bash: npm i -g bash-language-server
  ;; (add-hook 'sh-mode-hook 'eglot-ensure)
  ;; orderless
  (setq completion-category-overrides '((eglot (styles orderless))))
  (setq eglot-sync-connect nil)
  ;; longer timeout for eglot-jl
  (setq eglot-connect-timeout 600)
  ;; less verbose
  (fset #'jsonrpc--log-event #'ignore)
  (setq eglot-events-buffer-size 0)
  ;; eldoc
  ;; (add-hook 'eglot-managed-mode-hook
  ;;           (lambda ()
  ;;             (push #'flymake-eldoc-function eldoc-documentation-functions)
  ;;             (setq eldoc-documentation-strategy #'eldoc-documentation-compose)))
  ;; (setq eglot-stay-out-of '(eldoc-documentation-strategy)
  ;;       eldoc-documentation-strategy 'eldoc-documentation-compose)
  ;; )

;; reference: https://robbmann.io/posts/emacs-eglot-pyrightconfig/
(defun pyrightconfig-write (virtualenv)
  (interactive "DEnv: ")

  (let* (;; file-truename and tramp-file-local-name ensure that neither `~' nor
         ;; the Tramp prefix (e.g. "/ssh:my-host:") wind up in the final
         ;; absolute directory path.
         (venv-dir (tramp-file-local-name (file-truename virtualenv)))

         ;; Given something like /path/to/.venv/, this strips off the trailing `/'.
         (venv-file-name (directory-file-name venv-dir))

         ;; Naming convention for venvPath matches the field for
         ;; pyrightconfig.json.  `file-name-directory' gets us the parent path
         ;; (one above .venv).
         (venvPath (file-name-directory venv-file-name))

         ;; Grabs just the `.venv' off the end of the venv-file-name.
         (venv (file-name-base venv-file-name))

         ;; Eglot demands that `pyrightconfig.json' is in the project root
         ;; folder.
         (base-dir (vc-git-root default-directory))
         (out-file (expand-file-name "pyrightconfig.json" base-dir))

         ;; Finally, get a string with the JSON payload.
         (out-contents (json-encode (list :venvPath venvPath :venv venv))))

    ;; Emacs uses buffers for everything.  This creates a temp buffer, inserts
    ;; the JSON payload, then flushes that content to final `pyrightconfig.json'
    ;; location
    (with-temp-file out-file (insert out-contents))))


(provide 'init-eglot)
;;; init-eglot.el ends here
