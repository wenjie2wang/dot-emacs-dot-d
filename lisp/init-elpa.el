;;; package --- Summary
;;; Commentary:
;;; Code:

;; use emacs package manager
(require 'package)

;;; Install into separate package dirs for each Emacs version, to
;;; prevent bytecode incompatibility
(let ((versioned-package-dir
       (expand-file-name
	(format "elpa-%s.%s" emacs-major-version emacs-minor-version)
        user-emacs-directory)))
  (setq package-user-dir versioned-package-dir))

;; use melpa
(setq package-enable-at-startup nil)
(if *is-a-mac*
    (setq package-archives '(("melpa" . "~/local-melpa/melpa/")
                             ("org"   . "~/local-melpa/org/")
                             ("gnu"   . "~/local-melpa/gnu/")))
  (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/")
               ;; '("jcs-elpa" . "https://jcs-emacs.github.io/jcs-elpa/packages/")
               ;; '("stable-melpa" . "https://stable.melpa.org/packages/")
               )
  ;; (setq package-archive-priorities '(("melpa"    . 5)
  ;;                                    ("jcs-elpa" . 0)))
  )


(package-initialize)

;; bootstrap pkg: use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-always-ensure t)

;; hide some lighters
;; (use-package delight
;;   :ensure t)

;; pkg: try
;; (use-package try
;;   :ensure t)

(provide 'init-elpa)
;;; init-elpa.el ends here
