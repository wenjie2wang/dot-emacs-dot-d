;;; package --- Summary:
;;; Commentary:
;;; Code:

(use-package ess
  :config
  (require 'ess-site)
  ;; Turn off smart underscore in ESS
  ;; (setq ess-smart-S-assign-key nil) ; removed
  ;; ;; ESS Mode (.R file)
  (setq ess-eval-visibly 'nowait)
  (setq ess-history-file nil)
  (setq ess-R-font-lock-keywords
	    '((ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:constants  . t)
          (ess-R-fl-keyword:modifiers  . t)
          (ess-R-fl-keyword:fun-defs   . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)))
  (setq inferior-R-font-lock-keywords
	    '(;; comint is bad at prompt highlighting
	      (ess-S-fl-keyword:prompt . t)
          (ess-R-fl-keyword:keywords . t)
          (ess-R-fl-keyword:constants . t)
          (ess-R-fl-keyword:modifiers . t)
          (ess-R-fl-keyword:messages . t)
          (ess-R-fl-keyword:fun-defs . t)
          (ess-R-fl-keyword:assign-ops . t)
          (ess-fl-keyword:matrix-labels . t)
          (ess-fl-keyword:fun-calls . t)
          (ess-fl-keyword:numbers)
          (ess-fl-keyword:operators . t)
          (ess-fl-keyword:delimiters)
          (ess-fl-keyword:= . t)
          (ess-R-fl-keyword:F&T . t)))
  (setq ess-startup-directory 'default-directory)
  (setq inferior-R-args "--no-save --no-restore --no-site-file --no-environ")
  :bind (
	     :map ess-mode-map
	     ("C-r" . ess-eval-function-or-paragraph-and-step)
         ("C-c C-e" . ess-eval-region)
	     ("C-c M-." . xref-find-defitions)
	     ("M--" . " <- ")
	     ("C-\\" . " |> ")
	     ("M-0 M--" . nil)
         ("M-q" . fill-paragraph)
         :map inferior-ess-mode-map
	     ("M--" . " <- ")
	     ("C-\\" . " |> ")
	     ("M-0 M--" . nil)
	     )
  )

;;; disable flymake and flycheck for ess
(add-hook 'ess-mode-hook
          (lambda ()
            (add-hook 'xref-backend-functions #'dumb-jump-xref-activate nil t)
            (setq-local corfu-auto nil)
            (setq-local corfu-cycle t)
            (setq ess-use-flymake nil)
            (ess-set-style 'RRR)
            (setq ess-align-nested-calls nil) ; please indent ifelse as usual
            (setq flycheck-disabled-checkers '(r-lintr))))

(add-hook 'inferior-ess-mode-hook
          (lambda ()
            (setq-local corfu-auto nil)
            (setq-local corfu-cycle t)
            (add-to-list 'mode-line-process
                         '(:eval (nth ess--busy-count ess-busy-strings)))))

;; (add-hook 'ess-r-post-run-hook
;;           (lambda ()
;;             (setq patch-essr-dir (expand-file-name "R" user-emacs-directory))
;;             (ess-command (format "source('%s/patch_essr.R')" patch-essr-dir))
;;             ))

;; add busy-strings to doom-mode-line
;; (advice-add
;;  'inferior-ess-mode
;;  :after
;;  (lambda ()
;;    (add-to-list 'mode-line-process '(:eval (nth ess--busy-count ess-busy-strings)))))

;;; enable etags backend in addition to ess-r-xref-backend
;;; modified from Pan Xie's example at https://www.emacswiki.org/emacs/TagsFile
;; (when (require 'xref nil t)
;;   (defun xref--find-xrefs (input kind arg display-action)
;;     "Re-define the `xref--find-xrefs'.
;;  This is a re-defined version of `xref--find-xrefs'.  This
;;  function will call all backends until the definitions are found
;;  or the `xref-backend-functions' is exhausted."
;;     (let ((fn (intern (format "xref-backend-%s" kind)))
;;           (tail xref-backend-functions))
;;       (cl-block nil
;;         (while tail
;;           (let* ((backend-fn (car tail))
;;                  (backend (and (symbol-function backend-fn) (funcall backend-fn)))
;;                  (xrefs (and backend (funcall fn backend arg))))
;;             (when xrefs
;;               (cl-return (xref--show-xrefs xrefs display-action))))
;;           (setq tail (cdr tail)))
;;         (user-error "No %s found for: %s" (symbol-name kind) input)))))

;; add a hook function to the `ess-mode-hook':
;; (defun hook:ess-mode ()
;;   "Hook for ess-mode."
;;   (when (require 'xref nil t)
;;     (setq-local xref-backend-functions '(etags--xref-backend ess-r-xref-backend t))))
;; (add-hook 'ess-mode-hook 'hook:ess-mode)

;; https://github.com/emacs-ess/ESS/issues/947
(defun ess-format-region-as-r (beg end)
  "Format region of code R using the R parser."
  (interactive "r")
  (let ((string (replace-regexp-in-string
		         "\"" "\\\\\\&"
		         (replace-regexp-in-string ;; how to avoid this double matching?
		          "\\\\\"" "\\\\\\&" (buffer-substring-no-properties beg end))))
	    (buf (get-buffer-create "*ess-command-output*")))
    (ess-force-buffer-current "Process to load into:")
    (ess-command
     (format
      "local({oo <- options(keep.source = FALSE);
cat('\n', paste(deparse(parse(text = \"%s\")[[1L]]), collapse = '\n'), '\n', sep = '')
options(oo)})\n"
      string) buf)
    (with-current-buffer buf
      (goto-char (point-max))
      ;; (skip-chars-backward "\n")
      (let ((end (point)))
	    (goto-char (point-min))
	    (goto-char (1+ (point-at-eol)))
	    (setq string (buffer-substring-no-properties (point) end))
	    ))
    (delete-region beg end)
    (insert string)
    ))

(defun ess-reformat-region-with-formatr (beg end)
  "Format region of code R using formatR::tidy_source()."
  (interactive "r")
  (let ((string
         (replace-regexp-in-string
          "\"" "\\\\\\&"
          (replace-regexp-in-string ;; how to avoid this double matching?
           "\\\\\"" "\\\\\\&"
           (buffer-substring-no-properties beg end))))
	    (buf (get-buffer-create "*ess-command-output*")))
    (ess-force-buffer-current "Process to load into:")
    (ess-command
     (format                            ; R parser use 'width.cutoff = 60L'
      "local({formatR::tidy_source(text = \"\n%s\", arrow = TRUE, width.cutoff = 60L) })\n"
      string) buf)
    (with-current-buffer buf
      (goto-char (point-max))
      ;; (skip-chars-backward "\n")
      (let ((end (point)))
	    (goto-char (point-min))
	    (goto-char (1+ (point-at-eol)))
	    (setq string (buffer-substring-no-properties (point) end))
	    ))
    (delete-region beg end)
    (insert string)
    (delete-char -1)
    ))

(defun ess-format-region-with-styler (beg end)
  "Format region of code R using styler::style_text()."
  (interactive "r")
  (let ((string
         (replace-regexp-in-string
          "\"" "\\\\\\&"
          (replace-regexp-in-string ;; how to avoid this double matching?
           "\\\\\"" "\\\\\\&"
           (buffer-substring-no-properties beg end))))
	    (buf (get-buffer-create "*ess-command-output*")))
    (ess-force-buffer-current "Process to load into:")
    (ess-command
     (format
      "local({options(styler.colored_print.vertical = FALSE);
styler::style_text(text = \"\n%s\",
reindention = styler::specify_reindention(regex_pattern = \"###\", indention = 0),
indent_by = 4)})\n"
      string) buf)
    (with-current-buffer buf
      (goto-char (point-max))
      ;; (skip-chars-backward "\n")
      (let ((end (point)))
	    (goto-char (point-min))
	    (goto-char (1+ (point-at-eol)))
	    (setq string (buffer-substring-no-properties (point) end))
	    ))
    (delete-region beg end)
    (insert string)
    (delete-char -1)
    ))


;; stan-mode
;; (use-package stan-mode)

(provide 'init-ess)
;;; init-ess.el ends here
