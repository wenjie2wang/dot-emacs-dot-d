;;; init-everywhere.el --- Config for everywhere
;;; Commentary:
;;; Code:

;; use this package along with ghost-text
(use-package atomic-chrome
  :config
  (require 'atomic-chrome)
  (atomic-chrome-start-server)
  (setq atomic-chrome-extension-type-list '(ghost-text))
  (setq atomic-chrome-default-major-mode 'markdown-mode))
;; for edge, 1) super+G to activate; 2) C-G to disconnect


;; needs to creat shortcut
;; (use-package emacs-everywhere
;;   :ensure t)

(provide 'init-everywhere)
;;; init-everywhere.el ends here
