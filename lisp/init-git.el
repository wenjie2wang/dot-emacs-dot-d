(use-package diff-hl
  :config
  (setq diff-hl-disable-on-remote t)
  (add-hook 'after-init-hook 'global-diff-hl-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package magit
  :config
  (setq-default magit-diff-refine-hunk 'all)
  (setq magit-refresh-status-buffer nil)
  (setq auto-revert-buffer-list-filter
      'magit-auto-revert-repository-buffer-p)
  (global-set-key (kbd "C-x g") 'magit-status)
  (global-set-key (kbd "C-x M-g") 'magit-dispatch))

(use-package hl-todo
  :config
  (global-hl-todo-mode))

;; (use-package magit-todos
;;   :after (magit)
;;   :config
;;   (let ((inhibit-message t))
;;     (magit-todos-mode 1))
;;   (transient-append-suffix 'magit-status-jump '(0 0 -1)
;;     '("T " "Todos" magit-todos-jump-to-todos))
;;   (setq magit-todos-exclude-globs '(".git" "env" "pyenv" "venv" "renv"
;;                                     "*.html" "*.json" "*.map" "*.js"))
;;   (setq magit-todos-branch-list nil)
;;   (setq magit-todos-branch-list-merge-base-ref "main")
;;   )

(provide 'init-git)
