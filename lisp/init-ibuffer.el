;; pkg: ibuffer-vc
(use-package ibuffer-vc)

(defun ibuffer-set-up-preferred-filters ()
  (ibuffer-vc-set-filter-groups-by-vc-root)
  (unless (eq ibuffer-sorting-mode 'filename/process)
    (ibuffer-do-sort-by-filename/process)))

(add-hook 'ibuffer-hook 'ibuffer-set-up-preferred-filters)

(setq ibuffer-filter-group-name-face 'font-lock-doc-face)

(setq-default ibuffer-show-empty-filter-groups nil)

;; use ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

(provide 'init-ibuffer)
