;; pkg: swiper + ivy + counsel
(use-package ivy
  :delight
  :config
  (setq-default ivy-use-virtual-buffers t
                ivy-virtual-abbreviate 'fullpath
                ivy-count-format ""
                projectile-completion-system 'ivy
                ivy-magic-tilde nil
                ivy-dynamic-exhibit-delay-ms 150
                ivy-use-selectable-prompt t)
  (define-key ivy-minibuffer-map (kbd "RET") #'ivy-alt-done)
)
(use-package counsel
  :delight
  :init
  (add-hook 'after-init-hook 'counsel-mode))
(use-package swiper
  :bind (
         ;; ("C-s" . swiper)
         ;; ("C-r" . swiper)
         ;; ("C-c C-r" . ivy-resume)
         ("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-c g" . counsel-git)
         ("C-c j" . counsel-git-grep)
         ;; ("C-c k" . counsel-ag)
         ("C-x l" . counsel-locate)
         ;; ("C-S-o" . counsel-rhythmbox)
         )
  :config
  (ivy-mode 1)
  (setq enable-recursive-minibuffers t)
  ;; (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
  (setq-default ivy-initial-inputs-alist
                '((Man-completion-table . "^")
                  (woman . "^")))
  )

;; pkg: ivy-rich
(use-package ivy-rich
  :config
  (require 'ivy-rich)
  (ivy-rich-mode 1)
  (setq ivy-virtual-abbreviate 'abbreviate
        ivy-rich-switch-buffer-align-virtual-buffer nil
        ivy-rich-path-style 'abbrev))

(provide 'init-ivy)
;;; init-ivy.el ends here
