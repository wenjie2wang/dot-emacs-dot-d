;; julia-mode
(use-package julia-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))
)

(use-package eglot-jl
  :config
  (add-hook 'julia-mode-hook
            (lambda () (eglot-jl-init) (eglot-ensure)))
  )

;; https://github.com/joaotavora/eglot/discussions/1146
(defun eglot-format-buffer-on-save ()
  (add-hook 'before-save-hook #'eglot-format-buffer -10 t))

(add-hook 'julia-mode-hook #'eglot-format-buffer-on-save)

(use-package julia-repl
  :config
  (require 'julia-repl)
  (add-hook 'julia-mode-hook 'julia-repl-mode) ;; always use minor mode
  (set-language-environment "UTF-8")
  (julia-repl-set-terminal-backend 'vterm)
  )

;; (use-package julia-snail
;;   :ensure t
;;   :requires vterm
;;   :hook (julia-mode . julia-snail-mode)
;;   )

(provide 'init-julia)
;;; init-julia.el ends here
