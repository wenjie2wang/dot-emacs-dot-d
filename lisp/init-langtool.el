;; if languagetool is available
(if (executable-find "languagetool")
    (use-package langtool
      :config
      (setq langtool-java-classpath
      "/usr/share/languagetool:/usr/share/java/languagetool/*")
      (require 'langtool)
      (setq langtool-default-language "en-US")
      )
    )

(use-package eglot-grammarly
  :straight (:host github :repo "emacs-grammarly/eglot-grammarly")
  :defer t  ; defer package loading
  :init
  (defun eglot-grammarly-load ()
    "Load Grammarly LSP server for Eglot."
    (interactive)
    (require 'eglot-grammarly)
    (call-interactively #'eglot))
  ;; :hook ((text-mode markdown-mode latex-mode). (lambda ()
  ;;                                     (require 'eglot-grammarly)
  ;;                                     (eglot-ensure)))
  )

(provide 'init-langtool)
;;; init-langtool.el ends here
