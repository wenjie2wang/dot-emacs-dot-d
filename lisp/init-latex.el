;; using auctex
(use-package tex
  :straight auctex)

;; Turn on RefTeX in AUCTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; enable flyspell
(add-hook 'LaTeX-mode-hook 'flyspell-mode)

;; Activate nice interface between RefTeX and AUCTeX
(setq reftex-plug-into-AUCTeX t)
;; to insert eqref for equations
(setq reftex-label-alist '(AMSTeX))

(provide 'init-latex)
