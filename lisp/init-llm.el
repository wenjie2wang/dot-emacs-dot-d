;; for using large language models in Emacs

;; gptel
(use-package gptel
  :config
  ;; register a backend
  (gptel-make-ollama "tars-ollama"
    :host "192.168.0.144:11434"
    :stream t
    :models '(llama3.1:8b llama3.2))
  ;; set default backend
  (setq
   gptel-model 'llama3.2
   gptel-backend
   (gptel-make-ollama "Ollama"
     :host "localhost:11434"    ; where it's running
     :stream t                  ; stream responses
     :models '(llama3.1:8b llama3.2)    ; list of models
     )
   )
)

(provide 'init-llm)
;;; init-llm.el ends here
