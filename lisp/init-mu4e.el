;;; package --- Summary:
;;; Commentary:
;;; Code:

;;; try to locate mu4e
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

;;; soft require
(if (not (require 'mu4e nil t))
    ;; if true
    (message "mu4e not enabled.")

  ;; else
  (require 'smtpmail)
  (require 'mu4e-contrib)

  ;; send emails by msmtp
  (setq sendmail-program "/usr/bin/msmtp"
        message-send-mail-function 'message-send-mail-with-sendmail
        message-sendmail-f-is-evil t
        message-sendmail-extra-arguments '("--read-envelope-from")
        send-mail-function 'smtpmail-send-it
        user-full-name "Wenjie Wang")

  ;; make mu4e default mail-user-agent
  (setq mail-user-agent 'mu4e-user-agent)

  ;; set default path for attachments
  (setq mu4e-attachment-dir  "~/Downloads")

  ;; keep cc myself
  (setq mu4e-compose-keep-self-cc t)

  ;; set date format
  (setq mu4e-date-format-long "%Y-%m-%d %H:%M:%S")
  (setq mu4e-headers-date-format "%m%d%y %H:%M")
  ;; mu4e only shows names in From: by default
  ;; we want the addresses
  (setq mu4e-view-show-addresses t)

  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)

  (setq mu4e-view-image-max-width 600)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))

  ;; does not need for mu v1.4
  ;; (setq mu4e-maildir (expand-file-name "~/email"))

  (setq mu4e-contexts
        `(
          ,(make-mu4e-context
            :name "org"
            :enter-func (lambda () (mu4e-message "Entering wwenjie.org context"))
            :leave-func (lambda () (mu4e-message "Leaving wwenjie.org context"))
            :match-func (lambda (msg)
                          (when msg
                            (string-prefix-p
                             "/wang@wwenjie.org" (mu4e-message-field msg :maildir))))
            :vars '((mu4e-drafts-folder . "/wang@wwenjie.org/drafts")
                    (mu4e-sent-folder . "/wang@wwenjie.org/sent")
                    (mu4e-trash-folder . "/wang@wwenjie.org/trash")
                    (user-mail-address . "wang@wwenjie.org")
                    (smtpmail-default-smtp-server . "mail.privateemail.com")
                    (smtpmail-smtp-server . "mail.privateemail.com")
                    (smtpmail-smtp-user . "wang@wwenjie.org")
                    (smtpmail-stream-type . ssl)
                    (smtpmail-smtp-service . 465)
                    (message-signature . nil)
                     ;; . (concat "Best,\nWenjie\n")
                    ))

          ,(make-mu4e-context
            :name "Gmail"
            :enter-func (lambda () (mu4e-message "Entering Gmail context"))
            :leave-func (lambda () (mu4e-message "Leaving Gmail context"))
            :match-func (lambda (msg)
                          (when msg
                            (string-prefix-p
                             "/gmail" (mu4e-message-field msg :maildir))))
            :vars '((mu4e-drafts-folder . "/gmail/drafts")
                    (mu4e-sent-folder . "/gmail/sent")
                    (mu4e-trash-folder . "/gmail/trash")
                    (user-mail-address . "wjwang.stat@gmail.com")
                    (smtpmail-default-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-server . "smtp.gmail.com")
                    (smtpmail-smtp-user . "wjwang.stat@gmail.com")
                    (smtpmail-stream-type . starttls)
                    (smtpmail-smtp-service . 587)
                    (message-signature . nil)
                     ;; . (concat "Best,\nWenjie\n")
                    ))
          )
        )

  ;; compose with the current context if no context matches;
  (setq mu4e-compose-context-policy nil)

  (setq mu4e-get-mail-command (format "INSIDE_EMACS=%s mbsync -a" emacs-version)
        mu4e-html2text-command 'mu4e-shr2text
        mu4e-update-interval 600
        mu4e-headers-auto-update t
        ;; mu4e-compose-signature-auto-include nil
        )

  ;; don't save message to Sent Messages, Gmail/IMAP takes care of this
  (setq mu4e-sent-messages-behavior 'delete)
  ;; show images
  (setq mu4e-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))

  ;; spell check
  (add-hook 'mu4e-compose-mode-hook
            (defun my-do-compose-stuff ()
              "My settings for message composition."
              ;; disable diff-hl-mode
              (diff-hl-mode -1)
              (auto-fill-mode -1)
              (visual-line-mode 1)
              (flyspell-mode 1)
              (save-excursion (message-add-header
                               (concat "Cc: " user-mail-address "\n")))))

  ;; shortcuts for jumpdir
  (setq mu4e-maildir-shortcuts
        '(("/wang@wwenjie.org/inbox" . ?i)
          ("/gmail/inbox" . ?g)))

  ;; when replying, look kind of like gmail
  (setq message-citation-line-format "On %e %B %Y at %R %Z, %f wrote:\n")
  (setq message-citation-line-function 'message-insert-formatted-citation-line)

  ;; help prevent some uid errors
  (setq mu4e-change-filenames-when-moving t)

  ;; my key bindings
  (global-set-key (kbd "M-s m") 'mu4e)

  ;; ======================================================================
  ;; enable OrgMsg
  (setq mail-user-agent 'mu4e-user-agent)
  (use-package org-msg)
  (defconst wj/org-msg-style
    (let* ((font-family '(font-family . "Arial, Helvetica, sans-serif"))
           (font-size '(font-size . "10pt"))
           (font `(,font-family ,font-size))
           (line-height '(line-height . "1.5"))
           (bold '(font-weight . "bold"))
           (theme-color "#0071c5")
           (color `(color . ,theme-color))
           (table `(,@font (margin-top . "0px")))
           (ftl-number `(,@font ,color ,bold (text-align . "left")))
           (inline-modes '(asl c c++ conf cpp csv diff ditaa emacs-lisp
                               fundamental ini json makefile man org plantuml
                               python R sh xml))
           (inline-src `((color . ,(face-foreground 'default))
                         (background-color . ,(face-background 'default))))
           (code-src
            (mapcar (lambda (mode)
                      `(code ,(intern (concat "src src-" (symbol-name mode)))
                             ,inline-src))
                    inline-modes))
           (base-quote '((padding-left . "5px") (margin-left . "10px")
                         (margin-top . "10px") (margin-bottom . "1px")
                         (font-style . "italic") (background . "#f9f9f9")))
           (quote-palette '("#324e72" "#6a3a4c" "#7a4900" "#ff34ff"
                            "#ff4a46" "#008941" "#006fa6" "#a30059"
                            "#ffdbe5" "#000000" "#0000a6" "#63ffac"))
           (quotes
            (mapcar (lambda (x)
                      (let ((c (nth x quote-palette)))
                        `(blockquote ,(intern (format "quote%d" (1+ x)))
                                     (,@base-quote
                                      (color . ,c)
                                      (border-left . ,(concat "3px solid "
                                                              (org-msg-lighten c)))))))
                    (number-sequence 0 (1- (length quote-palette))))))
      `((del nil (,@font (color . "grey") (border-left . "none")
                         (text-decoration . "line-through") (margin-bottom . "0px")
                         (margin-top . "10px") (line-height . "1.5")))
        (a nil (,color))
        (a reply-header ((color . "black") (text-decoration . "none")))
        (div reply-header ((padding . "3.0pt 0in 0in 0in")
                           (border-top . "solid #e1e1e1 1.0pt")
                           (margin-bottom . "20px")))
        (span underline ((text-decoration . "underline")))
        (li nil (,@font ,line-height (margin-bottom . "0px")
                        (margin-top . "2px")))
        (nil org-ul ((list-style-type . "square")))
        (nil org-ol (,@font ,line-height (margin-bottom . "0px")
                            (margin-top . "0px") (margin-left . "30px")
                            (padding-top . "0px") (padding-left . "5px")))
        (nil signature (,@font (margin-bottom . "20px")))
        (blockquote quote0 ,(append base-quote '((border-left . "3px solid #ccc"))))
        ,@quotes
        (code nil (,font-size (font-family . "monospace") (background . "#f9f9f9")))
        ,@code-src
        (nil linenr ((padding-right . "1em")
                     (color . "black")
                     (background-color . "#aaaaaa")))
        (pre nil (,@inline-src
                  (padding . "4pt")
                  (margin . "4px")
                  ;; (background . "#eff0fe")
                  (font-size . "10pt")
                  (font-family . "monospace")))
        (pre example ((background . "#eee")
                      (color . "black")))
        (div org-src-container ((margin-top . "10px")))
        (nil figure-number ,ftl-number)
        (nil table-number)
        (caption nil ((text-align . "left")
                      (background . ,theme-color)
                      (color . "white")
                      ,bold))
        (nil t-above ((caption-side . "top")))
        (nil t-bottom ((caption-side . "bottom")))
        (nil listing-number ,ftl-number)
        (nil figure ,ftl-number)
        (nil org-src-name ,ftl-number)

        (table nil (,@table ,line-height (border-collapse . "collapse")))
        (th nil ((border . "1px solid white")
                 (background-color . ,theme-color)
                 (color . "white")
                 (padding-left . "10px") (padding-right . "10px")))
        (td nil (,@table (padding-left . "10px") (padding-right . "10px")
                         (background-color . "#f9f9f9") (border . "1px solid white")))
        (td org-left ((text-align . "left")))
        (td org-right ((text-align . "right")))
        (td org-center ((text-align . "center")))

        (div outline-text-4 ((margin-left . "15px")))
        (div outline-4 ((margin-left . "10px")))
        (h4 nil ((margin-bottom . "1px") (font-size . "10pt")
                 ,font-family))
        (h3 nil ((margin-bottom . "1px") (text-decoration . "underline")
                 ,color (font-size . "11pt")
                 ,font-family))
        (h2 nil ((margin-top . "20px") (margin-bottom . "20px")
                 (font-style . "italic") ,color (font-size . "11pt")
                 ,font-family))
        (h1 nil ((margin-top . "20px")
                 (margin-bottom . "20px") ,color (font-size . "12pt")
                 ,font-family))
        (p nil ((text-decoration . "none") (margin-bottom . "1px")
                (margin-top . "10px") (line-height . "1.5") ,font-size
                ,font-family))
        (div nil (,@font (line-height . "1.5"))))))

  (setq org-msg-enforce-css wj/org-msg-style)
  (setq org-msg-options "html-postamble:nil H:5 num:nil ^:{} toc:nil author:nil email:nil \\n:t"
        org-msg-startup "hidestars indent inlineimages"
        org-msg-greeting-fmt "\nHi%s,\n\n"
        org-msg-recipient-names '(("wang@wwenjie.org" . "Wenjie Wang"))
        org-msg-greeting-name-limit 3
        org-msg-default-alternatives '((new		. (text html))
                                       (reply-to-html	. (text html))
                                       (reply-to-text	. (text)))
        org-msg-convert-citation t
;;         org-msg-signature "

;; #+begin_signature
;; Best,
;; Wenjie
;; #+end_signature"
        )

  (org-msg-mode)                        ; enable org-msg

  ;; folding
  ;; (use-package
  ;;   mu4e-thread-folding
  ;;   :straight (mu4e-thread-folding
  ;;              :type git :host github :repo "rougier/mu4e-thread-folding")
  ;;   :after mu4e
  ;;   :config
  ;;   (add-to-list 'mu4e-header-info-custom
  ;;                '(:empty . (:name "Empty"
  ;;                                  :shortname ""
  ;;                                  :function (lambda (msg) "  "))))

  ;;   (setq mu4e-headers-fields '((:empty         .    2)
  ;;                               (:human-date    .   12)
  ;;                               (:flags         .    6)
  ;;                               (:mailing-list  .   10)
  ;;                               (:from          .   22)
  ;;                               (:subject       .   nil))
  ;;         mu4e-thread-folding-default-view 'folded
  ;;         mu4e-headers-found-hook '(mu4e-headers-mark-threads
  ;;                                   mu4e-headers-fold-all))
  ;;   )

  )


(provide 'init-mu4e)
;;; init-mu4e.el ends here
