;;; package --- Summary
;;; Commentary:
;;; Code:

;; pkg: org-mode
;; (require 'org)
(straight-use-package 'org)

(require 'ox)
(require 'ox-html)
(require 'ox-latex)

(use-package htmlize)

(use-package org-bullets
  :init
  (add-hook 'org-mode-hook #'org-bullets-mode))

(eval-after-load 'org
  '(progn
     ;; (org-indent-mode)
     ;; toggle C-TAB (org-force-cycle-archived) in org-mode
     (define-key org-mode-map [C-tab] nil)
     (define-key org-mode-map (kbd "C-c l") 'org-store-link)
     (define-key org-mode-map (kbd "C-c a") 'org-agenda)
     (define-key org-mode-map (kbd "C-c c") 'org-capture)
     (define-key org-mode-map (kbd "C-c b") 'org-iswitchb)
     (define-key org-mode-map (kbd "M-q") 'org-fill-paragraph)
     (org-babel-do-load-languages
      'org-babel-load-languages
      '((R . t)
        (julia . t)
        (C . t)
        (dot . t)
        (emacs-lisp . t)
        (gnuplot . t)
        (latex . t)
        (python . t)
        (awk . t)
        (css . t)
        (js . t)
        (lisp . t)
        (org . t)
        (shell . t)
        (sed . t)
        (sql . t)
        (sqlite . t)
        ))))

;; various preferences
(setq
 org-todo-keywords
 '((sequence "TODO(t)" "WIP(p)" "WAIT(w@/!)" "|"
             "DONE(d!)" "CANCELED(c@)"))
 org-catch-invisible-edits 'show
 org-edit-timestamp-down-means-later t
 org-export-coding-system 'utf-8
 org-export-kill-product-buffer-when-displayed t
 org-export-latex-default-packages-alist nil
 org-fast-tag-selection-single-key 'expert
 org-hide-emphasis-markers t
 ;; org-html-htmlize-output-type 'css
 org-html-validation-link nil
 org-latex-listings 'minted
 org-latex-packages-alist '(("" "minted"))
 org-latex-minted-options '(("frame" "lines") ("linenos=false"))
 org-latex-pdf-process '("latexmk -halt-on-error -shell-escape -pdf %f")
 org-log-done t
 org-reverse-note-order t
 org-src-fontify-natively t
 org-tags-column 68
 org-agenda-skip-scheduled-if-done t
 org-deadline-warning-days 14
 org-confirm-babel-evaluate nil
 org-display-remote-inline-images 'cache
 org-preview-latex-default-process 'dvisvgm
 org-highlight-latex-and-related '(latex)
 org-cite-export-processors '((beamer natbib)
                              (latex natbib) ; biblatex
                              (t csl)))

(setq org-format-latex-options
      (plist-put org-format-latex-options :scale 1.5))
(setq org-format-latex-options
      (plist-put org-format-latex-options :html-scale 0.75))


;; modified from the following
;; https://stackoverflow.com/questions/12717654/customizing-org-mode-exports
(defun org-dblock-write:insert-latex-macros (params)
  (let ((text)
        (file (or (plist-get params :file)
                  (expand-file-name "latex/macros.tex" user-emacs-directory))))
    (with-temp-buffer
      (insert-file file)
      (setq text (split-string (buffer-string) "\n" t)))
    (insert (mapconcat (lambda (str) (concat "#+LATEX_HEADER: " str))
                       text "\n"))
    (insert "\n#+BEGIN_HTML\n\\(\n")
    ;; (insert (mapconcat 'identity text "\n"))
    (insert (mapconcat (lambda (str)
                         (replace-regexp-in-string "usepackage" "require" str))
                       text "\n"))
    (insert "\n\\)\n#+END_HTML")))

;; to ignore #+begin_html when exporting org file to latex
(defun my-org-latex-ignore-html-blocks (data backend info)
  "Ignore HTML blocks when exporting to LaTeX."
  (when (org-export-derived-backend-p backend 'latex)
    (org-element-map data 'special-block
      (lambda (block)
        (when (string= (org-element-property :type block) "HTML")
          (org-element-extract-element block))))
    data))

(add-hook 'org-export-filter-parse-tree-functions
          'my-org-latex-ignore-html-blocks)


(if (file-exists-p "~/git/org-my-life")
    (setq org-agenda-files
          (directory-files-recursively "~/git/org-my-life/org/" "\\.org$")))

(use-package org-super-agenda
  :init
  (add-hook 'org-mode-hook #'org-super-agenda-mode)
  :config
  (setq org-agenda-span 14)
  (setq org-super-agenda-groups
        '((:auto-parent t)
          (:auto-priority t)))
  )

(use-package org-block-capf
  :straight (:host github :repo "xenodium/org-block-capf")
  :config
  (require 'org-block-capf)
  (add-hook 'org-mode-hook #'org-block-capf-add-to-completion-at-point-functions)
  )

;; Using straight:
(use-package corg
  :straight (:host github :repo "isamert/corg.el")
  :config
  (add-hook 'org-mode-hook #'corg-setup))

;; org-roam
(use-package org-roam
  :custom
  (org-roam-directory (file-truename "~/git/org-my-life/roam"))
  (org-roam-capture-templates
   '(
     ("d" "default" plain "%?"
      ;; :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}")
      :target
      (file+head
       "${slug}.org"
       "#+STARTUP: overview\n#+title: ${title}")
      :unnarrowed t)
     ("b" "bibliography references" plain "%?"
      :target
      (file+head
       "references/${citekey}.org"
       "#+FILETAGS: :reference:\n#+STARTUP: overview\n#+title: ${title}\n")
      :unnarrowed t)
     )
   )
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (setq org-roam-db-node-include-function
        (lambda ()
          (not (member "reference" (org-get-tags)))))
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  ;; (setq org-roam-extract-new-file-path "%<%Y%m%d%H%M%S>.org") ; I dislike long file names
  (setq org-roam-node-display-template
        (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  ;; (require 'org-roam-protocol)
  )

(provide 'init-org)
;;; init-org.el ends here
