;;; package --- Summary:
;;; Commentary:
;;; Code:

(use-package password-store)

;; gpg
(use-package epg
  :config
  (setq epg-pinentry-mode 'loopback))

(use-package pinentry
  :config
  (pinentry-start))

(provide 'init-pass)
;;; init-pass.el ends here
