;;; pdf-tools, nice but obviously slower than okular
(use-package pdf-tools
  :bind (:map pdf-view-mode-map
         ("M-v" . pdf-view-scroll-down-or-previous-page)
         ("C-v" . pdf-view-scroll-up-or-next-page))
  :config
  ;; (pdf-tools-install)
  (pdf-loader-install)

  ;; track if margin is trimmed
  (defvar is-pdf-view-set-slice-from-bounding-box 0)
  (advice-add 'pdf-view-set-slice-from-bounding-box
              :after (lambda () (setq is-pdf-view-set-slice-from-bounding-box 1)))
  (advice-add 'pdf-view-reset-slice
              :after (lambda () (setq is-pdf-view-set-slice-from-bounding-box 0)))
  (defun my-pdf-view-next-page ()
    (interactive)
    (call-interactively 'pdf-view-next-page-command)
    (when (eq is-pdf-view-set-slice-from-bounding-box 1)
      (pdf-view-set-slice-from-bounding-box)))
  (defun my-pdf-view-previous-page ()
    (interactive)
    (call-interactively 'pdf-view-previous-page-command)
    (when (eq is-pdf-view-set-slice-from-bounding-box 1)
      (pdf-view-set-slice-from-bounding-box)))
  (define-key pdf-view-mode-map (kbd "p") #'my-pdf-view-previous-page)
  (define-key pdf-view-mode-map (kbd "n") #'my-pdf-view-next-page)

  ;; trim margin consistently
  (defadvice pdf-view-scroll-up-or-next-page (after re-slice activate)
    (if (pdf-view-current-slice)
        (pdf-view-set-slice-from-bounding-box)))
  (defadvice pdf-view-scroll-down-or-previous-page (after re-slice activate)
    (if (pdf-view-current-slice)
        (pdf-view-set-slice-from-bounding-box)))
  (defadvice pdf-view-next-line-or-next-page (after re-slice activate)
    (if (pdf-view-current-slice)
        (pdf-view-set-slice-from-bounding-box)))
  (defadvice pdf-view-previous-line-or-previous-page (after re-slice activate)
    (if (pdf-view-current-slice)
        (pdf-view-set-slice-from-bounding-box)))
  )

(use-package saveplace-pdf-view
  :config
  (save-place-mode 1)
  )

(provide 'init-pdf)
;;; init.pdf.el ends here
