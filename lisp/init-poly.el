(use-package markdown-mode
  :mode ("\\.md\\'"))

;; require polymode
(use-package polymode)
(use-package poly-markdown
  :config
  (require 'poly-markdown))
(use-package poly-R
  :config (require 'poly-R))
(use-package poly-noweb
  :config
  (require 'poly-noweb)
  ;;; insert code chunk
;;; https://emacs.stackexchange.com/questions/27405/insert-code-chunk-in-r-markdown-with-yasnippet-and-polymode
  (defun rmd-insert-r-chunk (header)
    "Insert an r-chunk HEADER in markdown mode.
Necessary due to interactions between polymode and yas snippet."
    (interactive "sHeader: ")
    (insert (concat "```{r " header "}\n\n```"))
    (forward-line -1))
  )

;; Note that the following is not necessary to run quarto-mode! It's merely illustrating
;; how to associate different extensions to the mode.
(use-package quarto-mode
  :config
  (defun qmd-insert-r-chunk (header)
    "Insert an r-chunk HEADER in markdown mode.
Necessary due to interactions between polymode and yas snippet."
    (interactive "sHeader: ")
    (insert (concat "```{" header "}\n\n```"))
    (forward-line -1))
  )

(provide 'init-poly)
;;; init-poly.el ends here
