(use-package projectile
  :config
  (add-hook 'after-init-hook 'projectile-mode)
  ;; Shorter modeline
  (setq-default projectile-mode-line-prefix " ")
  ;; set prefix for projectile
  (define-key projectile-mode-map (kbd "M-s p") 'projectile-command-map)
  ;; (setq projectile-keymap-prefix (kbd "M-s p"))
  (setq projectile-use-git-grep t)
  (setq projectile-switch-project-action
      #'projectile-commander)
  (def-projectile-commander-method ?x
    "Open vterm for the project."
    (projectile-run-vterm))
  ;; to quickly open remote files using tramp
  (defadvice projectile-project-root (around ignore-remote first activate)
    (unless (file-remote-p default-directory) ad-do-it))
  )

;;; fix from https://github.com/bbatsov/projectile/issues/835
(add-hook 'text-mode-hook 'projectile-mode)

(provide 'init-projectile)
