;;; package --- Summary
;;; Commentary:
;;; Code:

(defvar python-shell-interpreter "python3")

;; (add-hook 'python-mode-hook
;;           (lambda ()
;;             (setq-default indent-tabs-mode t)
;;             (setq-default tab-width 4)
;; 			(setq-default python-indent-offset 4)
;;             (setq-default py-indent-tabs-mode t)))

;; (use-package anaconda-mode
;;   :ensure t
;;   :config
;;   (add-hook 'python-mode-hook 'anaconda-mode)
;;   (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
;;   )

;; (use-package company-anaconda
;;   :ensure t
;;   :config
;;   (with-eval-after-load 'company
;;     (with-eval-after-load 'python
;;       (add-to-list 'company-backends 'company-anaconda)))
;;   )

(use-package eval-in-repl
  :config
  (require 'eval-in-repl-python)
  (add-hook 'python-mode-hook
            #'(lambda ()
                (local-set-key (kbd "<C-return>") 'eir-eval-in-python)))
  (setq eir-use-python-shell-send-string nil)
  )

;; virtual environment
(use-package pyvenv
  :config
  (pyvenv-mode t)
  (pyvenv-tracking-mode t)

  ;; Set correct Python interpreter
  (setq pyvenv-post-activate-hooks
        (list (lambda ()
                (setq python-shell-interpreter (concat pyvenv-virtual-env "bin/python3")))))
  (setq pyvenv-post-deactivate-hooks
        (list (lambda ()
                (setq python-shell-interpreter "python3"))))
  )

(if (executable-find "pyenv")
    ;; pyenv for specific python version
    (use-package pyenv-mode
      :config
      (pyenv-mode)
      (unbind-key "C-c C-s" pyenv-mode-map))
  )

;; notebook
(use-package ein
  :config
  (setq ein:jupyter-server-use-subcommand "server"))

;; cython-mode for reading code
(use-package cython-mode)

(provide 'init-python)
;;; init-python.el ends here
