;;; init-selectrum.el --- Config for selectrum -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(use-package selectrum
  :config
  (add-hook 'after-init-hook 'selectrum-mode)
  (setq-default selectrum-fix-vertical-window-height t)
  (define-key selectrum-minibuffer-map (kbd "<C-backspace>")
    #'selectrum-backward-kill-sexp))

(use-package selectrum-prescient
  :config
  (require 'prescient)
  (prescient-persist-mode 1)
  (selectrum-prescient-mode 1)
  (global-set-key [remap execute-extended-command]
                  'execute-extended-command))

(use-package embark
  :config
  (define-key selectrum-minibuffer-map (kbd "C-c C-o") 'embark-export)
  (define-key selectrum-minibuffer-map (kbd "M-o") 'embark-act))

(use-package consult
  :config
  (setq-default consult-project-function 'projectile-project-root))

(when (executable-find "rg")
  (defun my-consult-ripgrep-at-point (&optional dir initial)
    (interactive (list prefix-arg (when-let ((s (symbol-at-point)))
                                    (symbol-name s))))
    (consult-ripgrep dir initial)))
(global-set-key (kbd "M-?") 'my-consult-ripgrep-at-point)

;; consult-buffer is too slow for tramp buffers
(defun my-consult-buffer ()
  "Not use consult-buffer for tramp buffers."
  (interactive (if (file-remote-p default-directory)
    (ivy-switch-buffer)
    (consult-buffer)
    )))
(global-set-key [remap switch-to-buffer] 'my-consult-buffer)
(global-set-key [remap switch-to-buffer-other-window] 'consult-buffer-other-window)
(global-set-key [remap switch-to-buffer-other-frame] 'consult-buffer-other-frame)
(global-set-key [remap goto-line] 'consult-goto-line)
(global-set-key [remap yank-pop] 'consult-yank-replace)

;; Hide all sources, except normal buffers in consult-buffer by default
(dolist (src consult-buffer-sources)
  (unless (eq src 'consult--source-buffer)
    (set src (plist-put (symbol-value src) :hidden t))))

(use-package embark-consult
  :config
  (with-eval-after-load 'embark
    (require 'embark-consult)
    (add-hook 'embark-collect-mode-hook 'embark-consult-preview-minor-mode)))

(use-package consult-flycheck)

(use-package marginalia
  :config
  (add-hook 'after-init-hook 'marginalia-mode)
  (setq-default marginalia-annotators '(marginalia-annotators-heavy)))

(provide 'init-selectrum)
;;; init-selectrum.el ends here
