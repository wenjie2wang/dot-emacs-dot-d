;;; use yasnippet-snippets
(use-package yasnippet
  :config (yas-global-mode 1))

(use-package yasnippet-snippets)

(provide 'init-snippet)
;;; init-snippet.el ends here
