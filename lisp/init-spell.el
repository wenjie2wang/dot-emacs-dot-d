(require 'ispell)

;; Add spell-checking in comments for all programming language modes
;; (add-hook 'prog-mode-hook 'flyspell-prog-mode)

(add-to-list 'safe-local-variable-values
             '(ispell-personal-dictionary . ".aspell.en.pws"))

(provide 'init-spell)
;;; init-spell.el ends here
