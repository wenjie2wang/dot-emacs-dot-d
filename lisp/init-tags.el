;;; using imenu-anywhere
;; (use-package imenu-anywhere
;;   :config
;;   ;; add a mark before running imenu-anywhere so that it is possible to go back
;;   ;; by C-u C-Space
;;   (global-set-key (kbd "C-.")
;;                   (lambda () (interactive)
;;                     (cua-set-mark)
;;                     (cua-set-mark)
;;                     (imenu-anywhere)))
;;   )

;;; using ggtags
;; (use-package ggtags
;;   :ensure t)
;; (add-hook 'c-mode-common-hook
;;           (lambda ()
;;             (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
;;               (ggtags-mode 1))))

;; https://github.com/jacktasia/dumb-jump
(use-package dumb-jump
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate)
  (setq xref-show-definitions-function #'xref-show-definitions-completing-read)
  (setq dumb-jump-force-searcher 'rg))


(provide 'init-tags)
