(use-package xterm-color
  :init
  (setq comint-output-filter-functions
        (remove 'ansi-color-process-output comint-output-filter-functions))
  (add-hook 'shell-mode-hook
          (lambda ()
            ;; Disable font-locking in this buffer to improve performance
            (font-lock-mode -1)
            ;; Prevent font-locking from being re-enabled in this buffer
            (make-local-variable 'font-lock-function)
            (setq font-lock-function (lambda (_) nil))
            (add-hook 'comint-preoutput-filter-functions 'xterm-color-filter nil t)))
  (add-hook 'inferior-ess-mode-hook
            (lambda () (add-hook 'comint-preoutput-filter-functions #'xterm-color-filter nil t)))
  :config
  (setq xterm-color-use-bold t))


;;; use vterm
(use-package vterm
  :config
  ;; # https://github.com/akermu/emacs-libvterm/issues/379
  ;; (add-to-list 'vterm-eval-cmds '("update-pwd" (lambda (path) (setq default-directory path))))
  (setq vterm-max-scrollback 100000)
  ;; (setq vterm-kill-buffer-on-exit nil)
  (defun starts-with-p (line prompt)
    "Check if LINE starts with PROMPT"
    (string= (substring line 0 (length prompt)) prompt)
    )
  (defun is-a-prompt-p (line)
    "Check if the LINE is the input line of some interactive process."
    (or
     (starts-with-p line "$")
     (starts-with-p line "gnuplot>")
     (starts-with-p line "julia>")
     (starts-with-p line ">>>")
     )
    )
  (defun send-command-input (command &optional args)
    "Check if the point is in the input line, if it is, send the
     command with vterm. This works only if the first characters
     of the line are in the database of is-a-prompt-p."
    (if (is-a-prompt-p (thing-at-point 'line))
        (vterm--self-insert)            ; Prompt line
      (funcall command args)
      )
    )
  (defun my-vterm-remove-empty-pairs ()
    "Remove right-char for empty pairs in vterm."
    (if (or (and (char-equal (following-char) ?\)) (char-equal (char-before) ?\())
            (and (char-equal (following-char) ?\}) (char-equal (char-before) ?\{))
            (and (char-equal (following-char) ?\]) (char-equal (char-before) ?\[))
            ;; (and (char-equal (following-char) ?\") (char-equal (char-before) ?\"))
            ;; (and (char-equal (following-char) ?\') (char-equal (char-before) ?\'))
            ;; (and (char-equal (following-char) ?\`) (char-equal (char-before) ?\`))
            )
        (vterm-send-C-d))
    )
  ;;; define shortcut for vterm-yank
  (defun set-vterm-keys ()
    "Set shortcuts for vterm"
    (define-key vterm-mode-map (kbd "C-v") 'cua-scroll-up)
    (define-key vterm-mode-map (kbd "M-v") 'cua-scroll-down)
    (define-key vterm-mode-map (kbd "C-y") 'vterm-yank)
    (define-key vterm-mode-map (kbd "M-p") 'vterm-send-C-p)
    (define-key vterm-mode-map (kbd "M-n") 'vterm-send-C-n)
    (define-key vterm-mode-map (kbd "M->") 'vterm-reset-cursor-point)
    (define-key vterm-mode-map (kbd "C-SPC")
      #'(lambda () (interactive) (send-command-input 'set-mark-command)))
    (define-key vterm-mode-map (kbd "C-f")
      #'(lambda () (interactive) (send-command-input 'forward-char)))
    (define-key vterm-mode-map (kbd "C-b")
      #'(lambda () (interactive) (send-command-input 'backward-char)))
    (define-key vterm-mode-map (kbd "C-l")
      #'(lambda () (interactive) (send-command-input 'recenter-top-bottom)))
    (define-key vterm-mode-map (kbd "C-n") nil)
    (define-key vterm-mode-map (kbd "C-p") nil)
    (define-key vterm-mode-map (kbd "M-s") nil)
    (define-key vterm-mode-map (kbd "C-s")
      #'(lambda () (interactive) (send-command-input 'isearch-forward)))
    (define-key vterm-mode-map (kbd "C-r")
      #'(lambda () (interactive) (send-command-input 'isearch-backward)))
    (define-key vterm-mode-map (kbd "M-f")
      #'(lambda () (interactive) (send-command-input 'forward-word)))
    (define-key vterm-mode-map (kbd "M-b")
      #'(lambda () (interactive) (send-command-input 'backward-word)))
    (define-key vterm-mode-map (kbd "C-e")
      #'(lambda () (interactive) (send-command-input 'move-end-of-line)))
    (define-key vterm-mode-map (kbd "C-a")
      #'(lambda () (interactive) (send-command-input 'move-beginning-of-line)))
    (define-key vterm-mode-map (kbd "M-w")
      #'(lambda () (interactive)
         (send-command-input
          '(lambda (interactive) (kill-ring-save (point) (mark t)))
          )))
    (define-key vterm-mode-map (kbd "(")
      #'(lambda () (interactive)
         (vterm-send-string "()")
         (vterm-send-left)
         ))
    (define-key vterm-mode-map (kbd "[")
      #'(lambda () (interactive)
         (vterm-send-string "[]")
         (vterm-send-left)
         ))
    (define-key vterm-mode-map (kbd "{")
      #'(lambda () (interactive)
         (vterm-send-string "{}")
         (vterm-send-left)
         ))
    ;; (define-key vterm-mode-map (kbd "\"")
    ;;   #'(lambda () (interactive)
    ;;      (vterm-send-string "\"\"")
    ;;      (vterm-send-left)
    ;;      ))
    ;; (define-key vterm-mode-map (kbd "'")
    ;;   #'(lambda () (interactive)
    ;;      (vterm-send-string "''")
    ;;      (vterm-send-left)
    ;;      ))
    ;; (define-key vterm-mode-map (kbd "`")
    ;;   #'(lambda () (interactive)
    ;;      (vterm-send-string "``")
    ;;      (vterm-send-left)
    ;;      ))
    (advice-add 'vterm-send-backspace
                :before (lambda () (my-vterm-remove-empty-pairs)))
    )
  (add-hook 'vterm-mode-hook #'set-vterm-keys))

(provide 'init-term)
;;; init-term.el ends here
