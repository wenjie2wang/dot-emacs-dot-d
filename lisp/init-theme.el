;; load theme - pkg: color-theme-sanityinc-tomorrow
(use-package color-theme-sanityinc-tomorrow
  :init
  (setq custom-safe-themes t)
  :config
  (require 'color-theme-sanityinc-tomorrow)
  (setq-default custom-enabled-themes '(sanityinc-tomorrow-eighties))
  (color-theme-sanityinc-tomorrow--define-theme eighties)
  (load-theme 'sanityinc-tomorrow-eighties t))

;; doom-themes
;; (use-package doom-themes
;;   :config
;;   ;; Global settings (defaults)
;;   (setq doom-themes-enable-bold nil)   ; if nil, bold is universally disabled
;;   (setq doom-themes-enable-italic nil) ; if nil, italics is universally disabled
;;   ;; (load-theme 'doom-zenburn t)
;;   (load-theme 'doom-one t)
;;   ;; Enable flashing mode-line on errors
;;   ;; (doom-themes-visual-bell-config)
;;   ;; Enable custom neotree theme (all-the-icons must be installed!)
;;   ;; (doom-themes-neotree-config)
;;   ;; or for treemacs users
;;   ;; (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;   ;; (doom-themes-treemacs-config)
;;   ;; Corrects (and improves) org-mode's native fontification.
;;   (doom-themes-org-config))


;; (use-package all-the-icons
;;   :config
;;   (setq inhibit-compacting-font-caches t)
;;   ;; show R icon for R scripts
;;   (add-to-list 'all-the-icons-mode-icon-alist
;;                '(ess-mode all-the-icons-fileicon "R" :face all-the-icons-lblue))
;;   (add-to-list 'all-the-icons-mode-icon-alist
;;                '(inferior-ess-r-mode all-the-icons-fileicon "R" :face all-the-icons-lblue))
;;   (add-to-list 'all-the-icons-mode-icon-alist
;;                '(inferior-ess-mode all-the-icons-fileicon "R" :face all-the-icons-lblue))
;;   )

(use-package nerd-icons
  :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  (nerd-icons-font-family "Symbols Nerd Font Mono")
  :config
  ;; show R icon for R scripts
  (add-to-list 'nerd-icons-mode-icon-alist
               '(ess-mode nerd-icons-mdicon "nf-md-language_r" :face nerd-icons-blue))
  (add-to-list 'nerd-icons-mode-icon-alist
               '(inferior-ess-r-mode nerd-icons-mdicon "nf-md-language_r" :face nerd-icons-blue))
  (add-to-list 'nerd-icons-mode-icon-alist
               '(inferior-ess-mode nerd-icons-mdicon "nf-md-language_r" :face nerd-icons-blue))
  (add-to-list 'nerd-icons-mode-icon-alist
               '(ess-r-transcript-mode nerd-icons-mdicon "nf-md-language_r" :face nerd-icons-blue))
  )

(provide 'init-theme)
