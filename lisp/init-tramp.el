;; for tramp
(require 'tramp)
(setq tramp-default-method "rsync")
(setq explicit-shell-file-name "/bin/bash")
(setq tramp-copy-size-limit nil)
(setq tramp-ssh-controlmaster-options
      "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=10")

;; necessary for using vterm with tramp
;; https://github.com/akermu/emacs-libvterm/issues/369
(setq tramp-shell-prompt-pattern
      "\\(?:^\\|\r\\)[^]#$%>\n]*#?[]#$%>].* *\\(^[\\[[0-9;]*[a-zA-Z] *\\)*")

;; (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;;; speed up completions
(setq tramp-completion-reread-directory-timeout t)
;;; disable version control to avoid delays:
(setq vc-ignore-dir-regexp
      (format "\\(%s\\)\\|\\(%s\\)"
              vc-ignore-dir-regexp
              tramp-file-name-regexp))
;; (setq vc-handled-backends '(Git))

;; dired with rsync
(use-package dired-rsync
  :bind (:map dired-mode-map
              ("C-c C-r" . dired-rsync)
              ("C-c C-x" . dired-rsync-transient)))

(provide 'init-tramp)
;;; init-tramp.el ends here
