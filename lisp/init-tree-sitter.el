;;; init-tree-sitter.el --- Config for tree-sitter
;;; Commentary:
;;; Code:


;; note: the following should be removed once emacs 29 is released

;; (use-package tree-sitter)

;; (use-package tree-sitter-langs
;;   :config
;;   (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))

;; (custom-set-variables '(font-lock-support-mode 'tree-sitter-lock-mode))
;; (global-tree-sitter-mode -1)

;; julia
;; (add-hook 'julia-mode-hook #'tree-sitter-mode)
;; (add-hook 'julia-mode-hook #'tree-sitter-hl-mode)

;; python
;; (add-hook 'python-mode-hook #'tree-sitter-mode)
;; (add-hook 'python-mode-hook #'tree-sitter-hl-mode)

(require 'treesit)

;; (add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
;; (add-to-list 'major-mode-remap-alist '(c-or-c++-mode . c-or-c++-ts-mode))

(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (c "https://github.com/tree-sitter/tree-sitter-c")
     (cpp "https://github.com/tree-sitter/tree-sitter-cpp")
     ;; (cmake "https://github.com/uyha/tree-sitter-cmake")
     ;; (css "https://github.com/tree-sitter/tree-sitter-css")
     ;; (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     ;; (go "https://github.com/tree-sitter/tree-sitter-go")
     ;; (html "https://github.com/tree-sitter/tree-sitter-html")
     ;; (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     ;; (json "https://github.com/tree-sitter/tree-sitter-json")
     (julia "https://github.com/tree-sitter/tree-sitter-julia")
     (make "https://github.com/alemuller/tree-sitter-make")
     ;; (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (r "https://github.com/r-lib/tree-sitter-r")
     ;; (toml "https://github.com/tree-sitter/tree-sitter-toml")
     ;; (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     ;; (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     ;; (yaml "https://github.com/ikatyang/tree-sitter-yaml")
     ))

;; install all by the following
;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))

;; use syntax highlight by ess for R

(use-package ts-fold
  :straight (ts-fold :type git :host github :repo "emacs-tree-sitter/ts-fold"))

;; for julia mode
(use-package julia-ts-mode
  :mode "\\.jl$")

(provide 'init-tree-sitter)
;;; init-tree-sitter.el ends here
