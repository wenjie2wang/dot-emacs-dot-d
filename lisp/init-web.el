;; web-mode
(use-package web-mode
  :mode
  (".twig$" ".html?$" ".hbs$" ".vue$" ".php$")
  :config
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-style-padding 2
        web-mode-script-padding 2
        web-mode-enable-auto-closing t
        web-mode-enable-auto-opening t
        web-mode-enable-auto-pairing t
        web-mode-enable-auto-indentation t)
  )

(provide 'init-web)
;;; init-web.el ends here
