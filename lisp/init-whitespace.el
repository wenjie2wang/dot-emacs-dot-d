(setq-default show-trailing-whitespace nil)

(defun show-trailing-whitespace ()
  "Enable display of trailing whitespace in this buffer."
  (setq-local show-trailing-whitespace t))

(dolist (hook '(prog-mode-hook text-mode-hook conf-mode-hook))
  (add-hook hook 'show-trailing-whitespace))

(use-package whitespace-cleanup-mode
  :config
  (add-hook 'after-init-hook 'global-whitespace-cleanup-mode))

(provide 'init-whitespace)
