;; pkg: ace-window
(use-package ace-window
  :config
  (global-set-key [remap other-window] 'ace-window)
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
        aw-background nil
        aw-scope 'frame))

;; mimic the window-switch with the tab-switch in web browser
(global-set-key [C-S-tab] #'(lambda () (interactive) (other-window -1)))
(global-set-key [C-tab] #'(lambda () (interactive) (other-window 1)))

;; split out a new window and switch to the next buffer
(global-set-key (kbd "C-x 3") (lambda () (interactive)
                                (split-window-horizontally)
                                (other-window 1)
                                (switch-to-next-buffer)
                                (other-window -1)))
(global-set-key (kbd "C-x 2") (lambda () (interactive)
                                (split-window-vertically)
                                (other-window 1)
                                (switch-to-next-buffer)
                                (other-window -1)))

(provide 'init-window)
